// eslint-disable-next-line import/no-extraneous-dependencies
import 'jest-preset-angular/setup-jest';
import * as $ from 'jquery';

Object.defineProperty(window, '$', { value: $ });
Object.defineProperty(global, '$', { value: $ });
Object.defineProperty(global, 'jQuery', { value: $ });
