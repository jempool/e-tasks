/* eslint-disable no-var */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor() { }

  activeComponent: string = 'departments';

  ngOnInit(): void { }

  setActiveComponent(component: string): void {
    this.activeComponent = component;
  }

  isActiveComponent(component: string): string {
    return (component === this.activeComponent) ? 'active' : '';
  }
}
