import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {
    this.APIUrl = environment.apiBaseURL;
  }

  readonly APIUrl: string;

  readonly headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });

  protected formatErrors(error: any) {
    return throwError(error.error);
  }

  getAll(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    const url = `${this.APIUrl}/${path}`;
    return this.http.get(url, { params, headers: this.headers }).pipe(catchError(this.formatErrors));
  }

  getBy(path:string, id: string, params: HttpParams = new HttpParams()): Observable<any> {
    const url = `${this.APIUrl}/${path}/${id}`;
    return this.http.get(url, { params }).pipe(catchError(this.formatErrors));
  }

  post(path:string, body:object, params: HttpParams = new HttpParams()): Observable<any> {
    const url = `${this.APIUrl}/${path}`;
    return this.http.post(url, body, { params }).pipe(catchError(this.formatErrors));
  }

  put(path:string, id: string, body:object, params: HttpParams = new HttpParams()): Observable<any> {
    const url = `${this.APIUrl}/${path}/${id}`;
    return this.http.put(url, body, { params }).pipe(catchError(this.formatErrors));
  }

  delete(path:string, id: string): Observable<any> {
    const url = `${this.APIUrl}/${path}/${id}`;
    return this.http.delete(url).pipe(catchError(this.formatErrors));
  }
}
