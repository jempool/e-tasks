import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EtasksService } from './etasks.service';

describe('EtasksService', () => {
  let service: EtasksService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(EtasksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
