import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/general/api/api.service';

@Injectable({
  providedIn: 'root',
})
export class EtasksService {
  constructor(private apiService: ApiService) { }

  getAll(): Observable<any[]> {
    return this.apiService.getAll('etasks');
  }

  getBy(etaskId: string): Observable<any> {
    return this.apiService.getBy('etasks', etaskId);
  }

  create(body: Object): Observable<any> {
    return this.apiService.post('etasks', body);
  }

  update(etaskId: string, body: Object): Observable<any> {
    return this.apiService.put('etasks', etaskId, body);
  }

  delete(etaskId: string): Observable<any> {
    return this.apiService.delete('etasks', etaskId);
  }
}
