import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/general/api/api.service';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  constructor(private apiService: ApiService) { }

  getAll(): Observable<any[]> {
    return this.apiService.getAll('employees/Details');
  }

  getBy(employeeId: string): Observable<any> {
    return this.apiService.getBy('employees', employeeId);
  }

  create(body: Object): Observable<any> {
    return this.apiService.post('employees', body);
  }

  update(employeeId: string, body: Object): Observable<any> {
    return this.apiService.put('employees', employeeId, body);
  }

  delete(employeeId: string): Observable<any> {
    return this.apiService.delete('employees', employeeId);
  }
}
