import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/general/api/api.service';

@Injectable({
  providedIn: 'root',
})
export class DepartmentsService {
  constructor(private apiService: ApiService) { }

  getAll(): Observable<any[]> {
    return this.apiService.getAll('departments');
  }

  getBy(departmentId: string): Observable<any> {
    return this.apiService.getBy('departments', departmentId);
  }

  create(body: Object): Observable<any> {
    return this.apiService.post('departments', body);
  }

  update(departmentId: string, body: Object): Observable<any> {
    return this.apiService.put('departments', departmentId, body);
  }

  delete(departmentId: string): Observable<any> {
    return this.apiService.delete('departments', departmentId);
  }
}
