export interface Employee {
  id: number
  name: string
  email: string
  departmentId: number
  departmentName?: string
  eTasks: any[]
}
