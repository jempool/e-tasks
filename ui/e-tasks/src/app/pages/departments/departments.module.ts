import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DepartmentsComponent } from './departments.component';
import { ShowDepComponent } from './components/show-dep/show-dep.component';
import { AddEditDepComponent } from './components/add-edit-dep/add-edit-dep.component';

@NgModule({
  declarations: [
    DepartmentsComponent,
    ShowDepComponent,
    AddEditDepComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
})
export class DepartmentsModule { }
