/* eslint-disable no-nested-ternary */
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DepartmentsService } from 'src/app/core/services/api/departments/departments.service';

@Component({
  selector: 'app-show-dep',
  templateUrl: './show-dep.component.html',
  styleUrls: ['./show-dep.component.scss'],
})
export class ShowDepComponent implements OnInit {
  constructor(private departmentsService: DepartmentsService, private toastrService: ToastrService) { }

  DepartmentList: any = [];

  ModalTitle: string = '';

  ActivateAddEditDepComp: boolean = false;

  varDep: any;

  DepartmentIdFilter: string = '';

  DepartmentNameFilter: string = '';

  DepartmentListWithoutFilter: any = [];

  ngOnInit(): void {
    this.refreshDepList();
  }

  addClick(): void {
    this.varDep = {
      DepartmentId: 0,
      DepartmentName: '',
    };

    this.ModalTitle = 'Add Department';
    this.ActivateAddEditDepComp = true;
  }

  editClick(item: any): void {
    this.varDep = item;
    this.ModalTitle = 'Edit Department';
    this.ActivateAddEditDepComp = true;
  }

  deleteClick(item: any): void {
    // eslint-disable-next-line no-restricted-globals
    // eslint-disable-next-line no-alert
    if (window.confirm('Are you sure?')) {
      this.departmentsService.delete(item.id).subscribe(() => {
        this.toastrService.success('Department successfully deleted', 'Success');
        this.refreshDepList();
      }, () => {
        this.toastrService.error('Department could not be deleted', 'Error');
      });
    }
  }

  closeClick(): void {
    this.ActivateAddEditDepComp = false;
    this.refreshDepList();
  }

  callbackFunctionCloseClick = (): void => {
    this.closeClick();
  };

  refreshDepList(): void {
    this.departmentsService.getAll().subscribe((data: any) => {
      this.DepartmentList = data;
      this.DepartmentListWithoutFilter = data;
    }, (error: any) => {
      console.log(error);
    });
  }

  filterFn(): void {
    const { DepartmentIdFilter } = this;
    const { DepartmentNameFilter } = this;

    this.DepartmentList = this.DepartmentListWithoutFilter.filter((el: any) => el.id.toString().toLowerCase()
      .includes(DepartmentIdFilter.toString().trim().toLowerCase()) && el.name.toString().toLowerCase()
      .includes(DepartmentNameFilter.toString().trim().toLocaleLowerCase()));
  }

  sortResult(prop:string, asc: boolean): void {
    this.DepartmentList = this.DepartmentListWithoutFilter.sort((a: any, b: any) => {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      }
      return (a[prop] < b[prop]) ? 1 : ((a[prop] > b[prop]) ? -1 : 0);
    });
  }
}
