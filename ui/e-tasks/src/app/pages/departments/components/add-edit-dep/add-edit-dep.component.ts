import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DepartmentsService } from 'src/app/core/services/api/departments/departments.service';

declare let $: any;

@Component({
  selector: 'app-add-edit-dep',
  templateUrl: './add-edit-dep.component.html',
  styleUrls: ['./add-edit-dep.component.scss'],
})
export class AddEditDepComponent implements OnInit {
  constructor(private departmentsService: DepartmentsService, private toastrService: ToastrService) { }

  @Input() inputDep: any = { id: 0, name: '' };

  @Input() callbackFunction: () => void = () => {};

  DepartmentId: number = 0;

  DepartmentName: string = '';

  ngOnInit(): void {
    this.DepartmentId = this.inputDep?.id;
    this.DepartmentName = this.inputDep?.name;
  }

  addDepartment() {
    const val = {
      id: this.inputDep?.id,
      name: this.inputDep?.name,
    };

    this.departmentsService.create(val).subscribe(() => {
      this.toastrService.success('Department successfully added', 'Success');
      this.callbackFunction();
    }, () => {
      this.toastrService.error('Department could not be added', 'Error');
    });

    $('.modal-content .close').click();
  }

  updateDepartment() {
    const val = {
      id: this.inputDep?.id,
      name: this.inputDep?.name,
    };

    this.departmentsService.update(this.inputDep.id, val).subscribe(() => {
      this.toastrService.success('Department successfully updated', 'Success');
      this.callbackFunction();
    }, () => {
      this.toastrService.error('Department could not be updated', 'Error');
    });

    $('.modal-content .close').click();
  }
}
