import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Department } from 'src/app/core/models/department.model';
import { DepartmentsService } from 'src/app/core/services/api/departments/departments.service';
import { EmployeesService } from 'src/app/core/services/api/employees/employees.service';

declare let $: any;

@Component({
  selector: 'app-add-edit-emp',
  templateUrl: './add-edit-emp.component.html',
  styleUrls: ['./add-edit-emp.component.scss'],
})
export class AddEditEmpComponent implements OnInit {
  constructor(
    private employeesService: EmployeesService,
    private departmentsService: DepartmentsService,
    private toastrService: ToastrService,
  ) { }

  @Input() inputEmp: any = { id: 0, name: '', email: '' };

  @Input() callbackFunction: () => void = () => {};

  EmployeeId: number = 0;

  EmployeeName: string = '';

  SelectedDepartmentName: string = '';

  SelectedDepartmentId: number = 0;

  DepartmentList: Department[] = [];

  ngOnInit(): void {
    this.EmployeeId = this.inputEmp?.id;
    this.EmployeeName = this.inputEmp?.name;
    this.SelectedDepartmentId = this.inputEmp?.departmentId;
    this.SelectedDepartmentName = this.inputEmp?.departmentName ?? '-- Select a Department --';

    this.departmentsService.getAll().subscribe((data: Department[]) => {
      this.DepartmentList = data;
    }, () => {
      this.toastrService.error('List of Employees could not be fetched', 'Error');
    });
  }

  selectValue(department: Department) {
    this.SelectedDepartmentName = department.name;
    this.SelectedDepartmentId = department.id;
  }

  addEmployee() {
    const val = {
      id: this.inputEmp?.id,
      name: this.inputEmp?.name,
      email: this.inputEmp.email,
      departmentId: this.SelectedDepartmentId,
      eTasks: [],
    };

    this.employeesService.create(val).subscribe(() => {
      this.toastrService.success('Employee successfully added', 'Success');
      this.callbackFunction();
    }, () => {
      this.toastrService.error('Employee could not be added', 'Error');
    });

    $('.modal-content .close').click();
  }

  updateEmployee() {
    const val = {
      id: this.inputEmp?.id,
      name: this.inputEmp?.name,
      email: this.inputEmp?.email,
      departmentId: this.SelectedDepartmentId,
      eTasks: this.inputEmp?.eTasks,
    };

    this.employeesService.update(this.inputEmp.id, val).subscribe(() => {
      this.toastrService.success('Employee successfully updated', 'Success');
      this.callbackFunction();
    }, () => {
      this.toastrService.error('Employee could not be updated', 'Error');
    });

    $('.modal-content .close').click();
  }
}
