/* eslint-disable no-nested-ternary */
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Employee } from 'src/app/core/models/employee.model';
import { EmployeesService } from 'src/app/core/services/api/employees/employees.service';

@Component({
  selector: 'app-show-emp',
  templateUrl: './show-emp.component.html',
  styleUrls: ['./show-emp.component.scss'],
})
export class ShowEmpComponent implements OnInit {
  constructor(private employeesService: EmployeesService, private toastrService: ToastrService) { }

  EmployeeList: any = [];

  ModalTitle: string = '';

  EmployeeTitle: string = '';

  ActivateAddEditEmpComp: boolean = false;

  ActivateAddEditETaskComp: boolean = false;

  varEmp: any;

  EmployeeIdFilter: string = '';

  EmployeeNameFilter: string = '';

  EmployeeDepartmentFilter: string = '';

  EmployeeEmailFilter: string = '';

  EmployeeListWithoutFilter: any = [];

  ngOnInit(): void {
    this.refreshEmpList();
  }

  addEmployee(): void {
    this.varEmp = {
      EmployeeId: 0,
      EmployeeName: '',
    };

    this.ModalTitle = 'Add Employee';
    this.ActivateAddEditEmpComp = true;
  }

  editEmployee(item: any): void {
    this.varEmp = item;
    this.ModalTitle = 'Edit Employee';
    this.ActivateAddEditEmpComp = true;
  }

  editETasks(item: any): void {
    this.varEmp = item;
    this.EmployeeTitle = item.name;
    this.ActivateAddEditETaskComp = true;
  }

  deleteEmployee(item: any): void {
    // eslint-disable-next-line no-restricted-globals
    // eslint-disable-next-line no-alert
    if (window.confirm('Are you sure?')) {
      this.employeesService.delete(item.id).subscribe(() => {
        this.toastrService.success('Employee successfully deleted', 'Success');
        this.refreshEmpList();
      }, () => {
        this.toastrService.error('Employee could not be deleted', 'Error');
      });
    }
  }

  closeClick(): void {
    this.ActivateAddEditEmpComp = false;
    this.ActivateAddEditETaskComp = false;
    this.refreshEmpList();
  }

  callbackFunctionCloseClick = (): void => {
    this.closeClick();
  };

  refreshEmpList(): void {
    this.employeesService.getAll().subscribe((data: Employee[]) => {
      this.EmployeeList = data;
      this.EmployeeListWithoutFilter = data;
    }, (error: any) => {
      console.log(error);
    });
  }

  filterFn(): void {
    const { EmployeeIdFilter } = this;
    const { EmployeeNameFilter } = this;
    const { EmployeeDepartmentFilter } = this;
    const { EmployeeEmailFilter } = this;

    this.EmployeeList = this.EmployeeListWithoutFilter.filter((el: any) => el.id.toString().toLowerCase()
      .includes(EmployeeIdFilter.toString().trim().toLowerCase())
      && el.name.toString().toLowerCase().includes(EmployeeNameFilter.toString().trim().toLocaleLowerCase())
      && el.departmentName.toString().toLowerCase().includes(EmployeeDepartmentFilter.toString().toLocaleLowerCase())
      && el.email.toString().toLowerCase().includes(EmployeeEmailFilter.toString().toLocaleLowerCase()));
  }

  sortResult(prop:string, asc: boolean): void {
    this.EmployeeList = this.EmployeeListWithoutFilter.sort((a: any, b: any) => {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      }
      return (a[prop] < b[prop]) ? 1 : ((a[prop] > b[prop]) ? -1 : 0);
    });
  }
}
