import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import { ShowEmpComponent } from './components/show-emp/show-emp.component';
import { AddEditEmpComponent } from './components/add-edit-emp/add-edit-emp.component';
import { EtasksModule } from '../etasks/etasks.module';

@NgModule({
  declarations: [
    EmployeesComponent,
    ShowEmpComponent,
    AddEditEmpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    EtasksModule,
  ],
})
export class EmployeesModule { }
