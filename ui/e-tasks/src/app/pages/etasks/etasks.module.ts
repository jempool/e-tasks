import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EtasksComponent } from './etasks.component';

@NgModule({
  declarations: [
    EtasksComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    EtasksComponent,
  ],
})
export class EtasksModule { }
