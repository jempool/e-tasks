import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Task } from 'src/app/core/models/task.model';
import { Employee } from 'src/app/core/models/employee.model';
import { EtasksService } from 'src/app/core/services/api/etasks/etasks.service';
import { EmployeesService } from 'src/app/core/services/api/employees/employees.service';

declare let $: any;

@Component({
  selector: 'app-etasks',
  templateUrl: './etasks.component.html',
  styleUrls: ['./etasks.component.scss'],
})
export class EtasksComponent implements OnInit {
  constructor(
    private etasksService: EtasksService,
    private employeesService: EmployeesService,
    private toastrService: ToastrService,
  ) { }

  @Input() inputTask: Task = { id: 0, description: '' };

  @Input() inputEmp: Employee = {
    id: 0, name: '', email: '', departmentId: 0, eTasks: [],
  };

  EmployeeId: number = 0;

  TaskList: Task[] = [];

  editingTask: boolean = false;

  ngOnInit(): void {
    this.refreshTaskList();
  }

  refreshTaskList(): void {
    this.employeesService.getBy(this.inputEmp.id.toString()).subscribe((data: Employee) => {
      this.TaskList = data.eTasks;
    }, (error: any) => {
      console.log(error);
    });
  }

  addTask(): void {
    this.etasksService.create(this.inputTask).subscribe((data: Task) => {
      this.TaskList.push(data);
      this.toastrService.success('Task successfully added', 'Success');
    }, () => {
      this.toastrService.error('Task could not be added', 'Error');
    });

    this.inputTask.description = '';
  }

  editTask(item: Task): void {
    this.editingTask = true;
    this.inputTask = item;
  }

  updateTaskModification(): void {
    this.etasksService.update(this.inputTask.id.toString(), this.inputTask).subscribe((data: Task) => {
      this.TaskList = this.TaskList.map((t) => {
        if (t.id === data.id) {
          return data;
        }
        return t;
      });
      this.toastrService.success('Task successfully udpated', 'Success');
    }, () => {
      this.toastrService.error('Task could not be udpated', 'Error');
    });

    this.editingTask = false;
    this.inputTask.description = '';
  }

  cancelTaskModification(): void {
    this.editingTask = false;
    this.inputTask.description = '';
    this.refreshTaskList();
  }

  deleteTask(item: any): void {
    // eslint-disable-next-line no-restricted-globals
    // eslint-disable-next-line no-alert
    if (window.confirm('Are you sure?')) {
      this.etasksService.delete(item.id).subscribe(() => {
        this.TaskList = this.TaskList.filter((t) => t.id !== item.id);
        this.toastrService.success('Task successfully deleted', 'Success');
      }, () => {
        this.toastrService.error('Task could not be deleted', 'Error');
      });
    }
  }

  saveBulkTasks(): void {
    this.inputEmp.eTasks = this.TaskList;
    this.employeesService.update(this.inputEmp.id.toString(), this.inputEmp).subscribe(() => {
      this.toastrService.success('All tasks successfully saved', 'Success');
    }, () => {
      this.toastrService.error('Tasks could not be saved', 'Error');
    });

    $('.modal-content .close').click();
  }

  cancelBulkTasks(): void {
    $('.modal-content .close').click();
  }
}
