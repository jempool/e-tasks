﻿using System.Threading.Tasks;
using ETasksApi.Application.Services.Employees;
using ETasksApi.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ETasksApi.Web.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeesService _employeesService;

        public EmployeesController(IEmployeesService employeesService)
        {
            this._employeesService = employeesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return this.Ok(await this._employeesService.GetAll());
        }

        [HttpGet("Details")]
        public async Task<IActionResult> GetAllDetails()
        {
            return this.Ok(await this._employeesService.GetAllDetails());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return this.Ok(await this._employeesService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(Employee employee)
        {
            return this.Ok(await this._employeesService.Create(employee));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Employee employee)
        {
            return this.Ok(await this._employeesService.Update(id, employee));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return this.Ok(await this._employeesService.Delete(id));
        }
    }
}
