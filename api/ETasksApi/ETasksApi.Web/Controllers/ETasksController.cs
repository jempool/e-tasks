﻿using System.Threading.Tasks;
using ETasksApi.Application.Services.ETasks;
using ETasksApi.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ETasksApi.Web.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ETasksController : ControllerBase
    {
        private readonly IETasksService _etasksService;

        public ETasksController(IETasksService etasksService)
        {
            this._etasksService = etasksService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return this.Ok(await this._etasksService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return this.Ok(await this._etasksService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(ETask etask)
        {
            return this.Ok(await this._etasksService.Create(etask));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, ETask etask)
        {
            return this.Ok(await this._etasksService.Update(id, etask));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return this.Ok(await this._etasksService.Delete(id));
        }
    }
}
