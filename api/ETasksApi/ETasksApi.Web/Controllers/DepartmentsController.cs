using System.Threading.Tasks;
using ETasksApi.Application.Services.Departments;
using ETasksApi.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ETasksApi.Web.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentsService _departmentsService;

        public DepartmentsController(IDepartmentsService departmentsService)
        {
            this._departmentsService = departmentsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return this.Ok(await this._departmentsService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return this.Ok(await this._departmentsService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(Department department)
        {
            return this.Ok(await this._departmentsService.Create(department));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Department department)
        {
            return this.Ok(await this._departmentsService.Update(id, department));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return this.Ok(await this._departmentsService.Delete(id));
        }
    }
}
