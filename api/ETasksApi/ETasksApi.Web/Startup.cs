using Coravel;
using ETasksApi.Application.Services.Departments;
using ETasksApi.Application.Services.Email;
using ETasksApi.Application.Services.Employees;
using ETasksApi.Application.Services.ETasks;
using ETasksApi.Application.Settings;
using ETasksApi.Domain.Repositories;
using ETasksApi.Infrastructure.Data;
using ETasksApi.Infrastructure.Data.EntityRepositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace ETasksApi.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddDatabaseDeveloperPageExceptionFilter();

            // Services
            services.TryAddScoped<IDepartmentsService, DepartmentsService>();
            services.TryAddScoped<IEmployeesService, EmployeesService>();
            services.TryAddScoped<IETasksService, ETasksService>();

            // Repositories
            services.TryAddScoped<IDepartmentsRepository, DepartmentsRepository>();
            services.TryAddScoped<IEmployeesRepository, EmployeesRepository>();
            services.TryAddScoped<IETasksRepository, ETasksRepository>();

            // Email
            services.Configure<MailSettings>(this.Configuration.GetSection("MailSettings"));
            services.TryAddSingleton(sp => sp.GetRequiredService<IOptions<MailSettings>>().Value);
            services.TryAddScoped<IEmailServiceUtils, EmailServiceUtils>();

            // Scheduler
            services.AddScheduler();
            services.TryAddTransient<SendEmailTaskListJob>();

            // services.Configure<TaskSchedulerSettings>(this.Configuration.GetSection("TaskSchedulerSettings"));
            services.TryAddSingleton(sp => sp.GetRequiredService<IOptions<TaskSchedulerSettings>>().Value);

            services.AddControllers();

            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ETasksApi", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ETasksApi v1"));
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ApplicationServices.UseScheduler(scheduler =>
            {
                var settings = this.Configuration.GetSection("TaskSchedulerSettings").Get<TaskSchedulerSettings>();
                scheduler.Schedule<SendEmailTaskListJob>().Cron(settings.SendTasksScheduler);
            });
        }
    }
}
