﻿using ETasksApi.Domain.Entities;

namespace ETasksApi.Domain.Repositories
{
    public interface IETasksRepository : IRepository<ETask>
    {
    }
}
