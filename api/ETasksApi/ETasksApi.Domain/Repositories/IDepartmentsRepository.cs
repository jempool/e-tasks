using ETasksApi.Domain.Entities;

namespace ETasksApi.Domain.Repositories
{
    public interface IDepartmentsRepository : IRepository<Department>
    {
    }
}