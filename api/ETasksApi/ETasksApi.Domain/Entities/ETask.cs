// using System.Collections.Generic;

namespace ETasksApi.Domain.Entities
{
    public class ETask
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
