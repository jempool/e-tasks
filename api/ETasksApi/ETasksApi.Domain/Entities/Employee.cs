using System.Collections.Generic;

namespace ETasksApi.Domain.Entities
{
    public class Employee
    {
        public Employee()
        {
            this.ETasks = new List<ETask>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? DepartmentId { get; set; }

        public virtual ICollection<ETask> ETasks { get; set; }
    }
}
