﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace ETasksApi.Infrastructure.Data.EntityRepositories
{
    public class ETasksRepository : IETasksRepository
    {
        private readonly AppDbContext _context;
        public ETasksRepository(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<ETask>> GetAll()
        {
            return await this._context.ETasks.ToArrayAsync();
        }

        public async Task<ETask> GetById(int id)
        {
            return await this._context.ETasks.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<ETask> Create(ETask etask)
        {
            await this._context.ETasks.AddAsync(etask);
            int changes = this._context.SaveChanges();

            return changes > 0 ? etask : null;
        }

        public async Task<ETask> Update(int id, ETask etask)
        {
            ETask currentETask = await this.GetById(id);

            if (currentETask is not null)
            {
                this._context.Entry(currentETask).CurrentValues.SetValues(etask);
                int changes = this._context.SaveChanges();

                return changes > 0 ? etask : null;
            }

            return null;
        }

        public async Task<ETask> Delete(int id)
        {
            ETask etask = await this.GetById(id);

            this._context.ETasks.Remove(etask);
            int changes = this._context.SaveChanges();

            return changes > 0 ? etask : null;
        }
    }
}
