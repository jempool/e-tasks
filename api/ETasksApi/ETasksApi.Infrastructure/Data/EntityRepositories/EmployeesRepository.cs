﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace ETasksApi.Infrastructure.Data.EntityRepositories
{
    public class EmployeesRepository : IEmployeesRepository
    {
        private readonly AppDbContext _context;
        public EmployeesRepository(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            return await this._context.Employees.Include("ETasks").ToArrayAsync();
        }

        public async Task<Employee> GetById(int id)
        {
            return await this._context.Employees.Include("ETasks").FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Employee> Create(Employee employee)
        {
            await this._context.Employees.AddAsync(employee);
            int changes = this._context.SaveChanges();

            return changes > 0 ? employee : null;
        }

        public async Task<Employee> Update(int id, Employee employee)
        {
            Employee existingEmployee = await this.GetById(id);

            if (existingEmployee != null)
            {
                this._context.Entry(existingEmployee).CurrentValues.SetValues(employee);

                foreach (var existingTask in existingEmployee.ETasks.ToList())
                {
                    if (!employee.ETasks.Any(c => c.Id == existingTask.Id))
                        this._context.ETasks.Remove(existingTask);
                }

                foreach (var etask in employee.ETasks)
                {
                    var existingTask = existingEmployee.ETasks
                        .Where(c => c.Id == etask.Id && c.Id != default(int))
                        .SingleOrDefault();

                    if (existingTask != null)
                    {
                        this._context.Entry(existingTask).CurrentValues.SetValues(etask);
                    }
                    else
                    {
                        var newTask = new ETask
                        {
                            Description = etask.Description,
                        };

                        existingEmployee.ETasks.Add(newTask);
                    }
                }

                int changes = this._context.SaveChanges();
                return changes > 0 ? employee : null;
            }

            return null;
        }

        public async Task<Employee> Delete(int id)
        {
            Employee employee = await this.GetById(id);

            this._context.Employees.Remove(employee);
            int changes = this._context.SaveChanges();

            return changes > 0 ? employee : null;
        }
    }
}