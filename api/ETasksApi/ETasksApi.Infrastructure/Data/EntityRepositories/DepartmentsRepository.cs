using System.Collections.Generic;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace ETasksApi.Infrastructure.Data.EntityRepositories
{
    public class DepartmentsRepository : IDepartmentsRepository
    {
        private readonly AppDbContext _context;
        public DepartmentsRepository(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Department>> GetAll()
        {
            return await this._context.Departments.ToArrayAsync();
        }

        public async Task<Department> GetById(int id)
        {
            return await this._context.Departments.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Department> Create(Department department)
        {
            await this._context.Departments.AddAsync(department);
            int changes = this._context.SaveChanges();

            return changes > 0 ? department : null;
        }

        public async Task<Department> Update(int id, Department department)
        {
            Department currentDeparment = await this.GetById(id);

            if (currentDeparment is not null)
            {
                this._context.Entry(currentDeparment).CurrentValues.SetValues(department);
                int changes = this._context.SaveChanges();

                return changes > 0 ? department : null;
            }

            return null;
        }

        public async Task<Department> Delete(int id)
        {
            Department department = await this.GetById(id);

            this._context.Departments.Remove(department);
            int changes = this._context.SaveChanges();

            return changes > 0 ? department : null;
        }
    }
}