using ETasksApi.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ETasksApi.Infrastructure.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<ETask> ETasks { get; set; }

        // ToDo: Define the details of Entities
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Department>()
                .HasMany(p => p.Employees);

            modelBuilder
                .Entity<Employee>()
                .HasMany(p => p.ETasks);
        }
    }
}