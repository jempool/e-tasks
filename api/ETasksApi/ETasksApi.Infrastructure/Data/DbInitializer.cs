/*using System.Collections.Generic;
using ETasksApi.Domain.Entities;*/

namespace ETasksApi.Infrastructure.Data
{
    public static class DbInitializer
    {
        public static void Initialize(AppDbContext context)
        {
/*            context.Database.EnsureCreated();

            var etasks = new ETask[]
{
                new ETask { Description = "Task 1" },
                new ETask { Description = "Task 2" },
                new ETask { Description = "Task 3" },
                new ETask { Description = "Task 4" },
                new ETask { Description = "Task 5" },
                new ETask { Description = "Task 6" },
                new ETask { Description = "Task 7" },
                new ETask { Description = "Task 8" },
                new ETask { Description = "Task 9" },
                new ETask { Description = "Task 10" },
                new ETask { Description = "Task 11" },
                new ETask { Description = "Task 12" },
                new ETask { Description = "Task 13" },
                new ETask { Description = "Task 14" },
                new ETask { Description = "Task 15" },
                new ETask { Description = "Task 16" },
                new ETask { Description = "Task 17" },
                new ETask { Description = "Task 18" },
                new ETask { Description = "Task 19" },
                new ETask { Description = "Task 20" },
                new ETask { Description = "Task 21" },
};

            foreach (ETask t in etasks)
            {
                context.ETasks.Add(t);
            }

            context.SaveChanges();

            var employees = new Employee[]
            {
                new Employee { Name = "Alexander Carson", Email = "A.Carson@acme.com", ETasks = new List<ETask> { etasks[16], etasks[17] } },
                new Employee { Name = "Alonso Meredith", Email = "A.Meredith@acme.com", ETasks = new List<ETask> { etasks[15] } },
                new Employee { Name = "Arturo Anand", Email = "A.Anand@acme.com" },
                new Employee { Name = "Gytis Barzdukas", Email = "G.Barzdukas@acme.com", ETasks = new List<ETask> { etasks[12], etasks[13], etasks[14] } },
                new Employee { Name = "Yan Li", Email = "Y.Li@acme.com", ETasks = new List<ETask> { etasks[10], etasks[11], etasks[18], etasks[19], etasks[20] } },
                new Employee { Name = "Peggy Justice", Email = "P.Justice@acme.com", ETasks = new List<ETask> { etasks[5], etasks[6], etasks[7], etasks[8], etasks[9] } },
                new Employee { Name = "Laura Norman", Email = "Lazy.Norman@acme.com", ETasks = new List<ETask> { } },
                new Employee { Name = "Nino Smith", Email = "N.Smith@acme.com", ETasks = new List<ETask> { etasks[0], etasks[1], etasks[2], etasks[3], etasks[4] } },
            };

            foreach (Employee e in employees)
            {
                context.Employees.Add(e);
            }

            context.SaveChanges();

            var departments = new Department[]
            {
                new Department { Name = "IT", Employees = new List<Employee> { employees[0], employees[1] } },
                new Department { Name = "Finance", Employees = new List<Employee> { employees[2], employees[3] } },
                new Department { Name = "Support", Employees = new List<Employee> { employees[4], employees[5] } },
                new Department { Name = "Sales", Employees = new List<Employee> { employees[6], employees[7] } },
            };

            foreach (Department d in departments)
            {
                context.Departments.Add(d);
            }

            context.SaveChanges();*/
        }
    }
}