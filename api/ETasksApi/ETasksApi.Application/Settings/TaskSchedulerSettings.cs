﻿namespace ETasksApi.Application.Settings
{
    public class TaskSchedulerSettings
    {
        public string SendTasksScheduler { get; set; }
    }
}