﻿namespace ETasksApi.Application.Settings
{
    public class MailSettings
    {
        public string SMTPSender { get; set; }

        public string SMTPServer { get; set; }

        public int SMTPPort { get; set; }

        public string SMTPUser { get; set; }

        public string SMTPPassword { get; set; }
    }
}
