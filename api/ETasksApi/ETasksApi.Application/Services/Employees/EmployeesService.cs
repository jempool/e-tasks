﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;

namespace ETasksApi.Application.Services.Employees
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IEmployeesRepository _employeesRepository;

        private readonly IDepartmentsRepository _departmentsRepository;

        public EmployeesService(IEmployeesRepository employeesRepository, IDepartmentsRepository departmentsRepository)
        {
            this._employeesRepository = employeesRepository;
            this._departmentsRepository = departmentsRepository;
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            return await this._employeesRepository.GetAll();
        }

        public async Task<IEnumerable<object>> GetAllDetails()
        {
            var employeeList = await this._employeesRepository.GetAll();
            var departmentList = await this._departmentsRepository.GetAll();

            var employeesDetails = (from employee in employeeList
                     join department in departmentList on employee.DepartmentId equals department.Id
                     orderby employee.Id ascending
                     select new
                     {
                         Id = employee.Id,
                         Email = employee.Email,
                         Name = employee.Name,
                         DepartmentId = employee.DepartmentId,
                         DepartmentName = department.Name,
                         ETasks = employee.ETasks,
                     }).ToList();

            return employeesDetails;
        }

        public async Task<Employee> GetById(int id)
        {
            return await this._employeesRepository.GetById(id);
        }

        public async Task<Employee> Create(Employee employee)
        {
            return await this._employeesRepository.Create(employee);
        }

        public async Task<Employee> Update(int id, Employee model)
        {
            return await this._employeesRepository.Update(id, model);
        }

        public async Task<Employee> Delete(int id)
        {
            return await this._employeesRepository.Delete(id);
        }
    }
}