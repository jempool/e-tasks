﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;

namespace ETasksApi.Application.Services.Employees
{
    public interface IEmployeesService : IService<Employee>
    {
        Task<IEnumerable<object>> GetAllDetails();
    }
}