using System.Collections.Generic;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;

namespace ETasksApi.Application.Services.Departments
{
   public class DepartmentsService : IDepartmentsService
   {
       private readonly IDepartmentsRepository _departmentsRepository;

       public DepartmentsService(IDepartmentsRepository departmentsRepository)
       {
           this._departmentsRepository = departmentsRepository;
       }

       public async Task<IEnumerable<Department>> GetAll()
        {
            return await this._departmentsRepository.GetAll();
        }

       public async Task<Department> GetById(int id)
       {
           return await this._departmentsRepository.GetById(id);
       }

       public async Task<Department> Create(Department department)
       {
           return await this._departmentsRepository.Create(department);
       }

       public async Task<Department> Update(int id, Department model)
       {
           return await this._departmentsRepository.Update(id, model);
       }

       public async Task<Department> Delete(int id)
       {
           return await this._departmentsRepository.Delete(id);
       }
    }
}