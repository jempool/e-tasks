using ETasksApi.Domain.Entities;

namespace ETasksApi.Application.Services.Departments
{
    public interface IDepartmentsService : IService<Department>
    {
    }
}