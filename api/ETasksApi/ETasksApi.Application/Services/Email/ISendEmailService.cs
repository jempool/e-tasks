﻿using System.Threading.Tasks;

namespace ETasksApi.Application.Services.Email
{
    public interface ISendEmailService
    {
        Task SendEmailAsync();
    }
}
