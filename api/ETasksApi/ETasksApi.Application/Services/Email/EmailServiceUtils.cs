﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ETasksApi.Domain.Entities;

namespace ETasksApi.Application.Services.Email
{
    public class EmailServiceUtils : IEmailServiceUtils
    {
        private const string HTMLFILEPATH = @"..\ETasksApi.Application\HTMLTemplates\emailTemplate.html";
        private string _template = string.Empty;

        public EmailServiceUtils()
        {
            this.InitializeHTMLTemplate();
        }

        public void InitializeHTMLTemplate()
        {
            using (StreamReader streamReader = new StreamReader(HTMLFILEPATH, Encoding.UTF8))
            {
                this._template = streamReader.ReadToEnd();
            }
        }

        public string AddTaskListToHTMLTemplate(ICollection<ETask> taskList)
        {
            string pattern = @"\bTaskList\b";
            string replace = string.Concat(taskList.Select(x => $"<li>{x.Description}</li>"));
            string result = Regex.Replace(this._template, pattern, replace);

            return result;
        }
    }
}
