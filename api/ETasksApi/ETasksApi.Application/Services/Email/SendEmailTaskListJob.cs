﻿using System.Threading.Tasks;
using Coravel.Invocable;
using ETasksApi.Application.Services.Employees;
using ETasksApi.Application.Settings;
using ETasksApi.Domain.Entities;
using HandlebarsDotNet;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;

namespace ETasksApi.Application.Services.Email
{
    public class SendEmailTaskListJob : ISendEmailService, IInvocable
    {
        private const string EMAILSUBJECT = "Task List for Today";
        private readonly MailSettings _appSettings;
        private readonly IEmailServiceUtils _emailServiceUtils;
        private readonly IEmployeesService _employeesService;

        public SendEmailTaskListJob(
            MailSettings appSettings,
            IEmailServiceUtils emailServiceUtils,
            IEmployeesService employeesService)
        {
            this._appSettings = appSettings;
            this._emailServiceUtils = emailServiceUtils;
            this._employeesService = employeesService;
        }

        public Task Invoke()
        {
            return Task.Run(() => this.SendEmailAsync());
        }

        public async Task SendEmailAsync()
        {
            var employeeList = await this._employeesService.GetAll();

            foreach (Employee employee in employeeList)
            {
                // Read Employee information
                var data = new { EmployeeName = employee.Name };

                // Populate the HTML template with the task list
                var htmlList = this._emailServiceUtils.AddTaskListToHTMLTemplate(employee.ETasks);
                var template = FillInTemplate(htmlList, data);

                // Create message
                var email = new MimeMessage();
                email.From.Add(MailboxAddress.Parse(this._appSettings.SMTPSender));
                email.To.Add(MailboxAddress.Parse(employee.Email));
                email.Subject = EMAILSUBJECT;
                email.Body = new TextPart(TextFormat.Html) { Text = template };

                // Send email
                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(this._appSettings.SMTPServer, this._appSettings.SMTPPort, SecureSocketOptions.StartTls);
                    smtp.Authenticate(this._appSettings.SMTPUser, this._appSettings.SMTPPassword);
                    smtp.Send(email);
                    smtp.Disconnect(true);
                }
            }
        }

        private static string FillInTemplate(string template, object data)
        {
            var compiledTemplate = Handlebars.Compile(template);
            var filledTemplate = compiledTemplate(data);
            return filledTemplate;
        }
    }
}
