﻿using System.Collections.Generic;
using ETasksApi.Domain.Entities;

namespace ETasksApi.Application.Services.Email
{
    public interface IEmailServiceUtils
    {
        public void InitializeHTMLTemplate();

        public string AddTaskListToHTMLTemplate(ICollection<ETask> taskList);
    }
}