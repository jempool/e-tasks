using System.Collections.Generic;
using System.Threading.Tasks;

namespace ETasksApi.Application.Services
{
    public interface IService<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<T> Create(T model);
        Task<T> Update(int id, T model);
        Task<T> Delete(int id);
    }
}