﻿using ETasksApi.Domain.Entities;

namespace ETasksApi.Application.Services.ETasks
{
    public interface IETasksService : IService<ETask>
    {
    }
}
