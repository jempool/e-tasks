﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ETasksApi.Domain.Entities;
using ETasksApi.Domain.Repositories;

namespace ETasksApi.Application.Services.ETasks
{
    public class ETasksService : IETasksService
    {
        private readonly IETasksRepository _etasksRepository;

        public ETasksService(IETasksRepository etasksRepository)
        {
            this._etasksRepository = etasksRepository;
        }

        public async Task<IEnumerable<ETask>> GetAll()
        {
            return await this._etasksRepository.GetAll();
        }

        public async Task<ETask> GetById(int id)
        {
            return await this._etasksRepository.GetById(id);
        }

        public async Task<ETask> Create(ETask etask)
        {
            return await this._etasksRepository.Create(etask);
        }

        public async Task<ETask> Update(int id, ETask model)
        {
            return await this._etasksRepository.Update(id, model);
        }

        public async Task<ETask> Delete(int id)
        {
            return await this._etasksRepository.Delete(id);
        }
    }
}
