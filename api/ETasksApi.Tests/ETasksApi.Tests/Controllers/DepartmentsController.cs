using Moq;
using Xunit;
using ETasksApi.Application.Services.Departments;
using ETasksApi.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ETasksApi.Web.Controllers
{
    public class UnitTestDepartmentsController
    {
        public Mock<IDepartmentsService> mock = new();
        private readonly IEnumerable<Department> _iEnumDepartments;
        private readonly Task<IEnumerable<Department>> _taskIEnumDepartments;
        private readonly DepartmentsController _departmentsController;

        private readonly int _departmentID = 1;

        private readonly List<Department> _departmentsList = new()
        {
            new Department { Name = "IT", Employees = new List<Employee> { new Employee { Name = "Alexander Carson", Email = "A.Carson@acme.com" } } },
            new Department { Name = "Finance", Employees = new List<Employee> { new Employee { Name = "Alexander Carson", Email = "A.Carson@acme.com" } } },
            new Department { Name = "Support", Employees = new List<Employee> { new Employee { Name = "Alexander Carson", Email = "A.Carson@acme.com" } } },
            new Department { Name = "Sales", Employees = new List<Employee> { new Employee { Name = "Alexander Carson", Email = "A.Carson@acme.com" } } },
        };

        private readonly Department _department = new Department { Name = "IT" };

        public UnitTestDepartmentsController()
        {
            this._iEnumDepartments = _departmentsList;
            this._taskIEnumDepartments = Task.Run(() => this._iEnumDepartments);
            
            mock.Setup(p => p.GetAll()).Returns(this._taskIEnumDepartments);
            mock.Setup(p => p.GetById(It.IsAny<int>())).Returns(Task.Run(() => this._department));
            mock.Setup(p => p.Create(It.IsAny<Department>())).Returns(Task.Run(() => this._department));
            mock.Setup(p => p.Update(It.IsAny<int>(), It.IsAny<Department>())).Returns(Task.Run(() => this._department));
            mock.Setup(p => p.Delete(It.IsAny<int>())).Returns(Task.Run(() => this._department));

            this._departmentsController = new(mock.Object); 
        }

        [Fact]
        public async Task Test_GetAll_ReturnsNotNull()
        {
            var actionResult = await this._departmentsController.GetAll();
            var okResult = actionResult as OkObjectResult;
            
            Assert.NotNull(okResult);
        }

        [Fact]
        public async Task Test_GetAll_ReturnsOkResult()
        {
            var actionResult = await this._departmentsController.GetAll();
            var okResult = actionResult as OkObjectResult;
            
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_GetAll_ReturnsDepartmentsList()
        {
            var actionResult = await this._departmentsController.GetAll();
            var okResult = actionResult as OkObjectResult;
            
            Assert.Equal(this._iEnumDepartments, okResult.Value);
        }

        [Fact]
        public async Task Test_GetById_ReturnsNotNull()
        {
            var actionResult = await this._departmentsController.GetById(this._departmentID);
            var okResult = actionResult as OkObjectResult;
            
            Assert.NotNull(okResult);
        }

        [Fact]
        public async Task Test_GetById_ReturnsOkResult()
        {
            var actionResult = await this._departmentsController.GetById(this._departmentID);
            var okResult = actionResult as OkObjectResult;
            
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_GetById_ReturnsDepartment()
        {
            var actionResult = await this._departmentsController.GetById(this._departmentID);
            var okResult = actionResult as OkObjectResult;
            
            Assert.Equal(this._department, okResult.Value);
        }

        [Fact]
        public async Task Test_CreateDepartment_ReturnsOkResult()
        {
            var actionResult = await this._departmentsController.Create(this._department);
            var okResult = actionResult as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_CreateDepartment_ReturnsDepartment()
        {
            var actionResult = await this._departmentsController.Create(this._department);
            var okResult = actionResult as OkObjectResult;
            
            Assert.Equal(this._department, okResult.Value);
        }

        [Fact]
        public async Task Test_UpdateDepartment_ReturnsOkResult()
        {
            var actionResult = await this._departmentsController.Update(this._departmentID, this._department);
            var okResult = actionResult as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_UpdateDepartment_ReturnsDepartment()
        {
            var actionResult = await this._departmentsController.Update(this._departmentID, this._department);
            var okResult = actionResult as OkObjectResult;

            Assert.Equal(this._department, okResult.Value);
        }

        [Fact]
        public async Task Test_DeleteDepartment_ReturnsOkResult()
        {
            var actionResult = await this._departmentsController.Delete(this._departmentID);
            var okResult = actionResult as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_DeleteDepartment_ReturnsDepartment()
        {
            var actionResult = await this._departmentsController.Delete(this._departmentID);
            var okResult = actionResult as OkObjectResult;

            Assert.Equal(this._department, okResult.Value);
        }
    }
}
